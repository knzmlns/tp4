# 0 Prérequisites
* Utilisation de la box  Centos 7 faite au préalable dans le tp2 en un peu modifiée

* Name 	IP 	Rôle
* node1.tp4.gitea 	192.168.1.11 	    Gitea
* node1.tp4.mariadb 192.168.1.12 	    Base de donnée
* node1.tp4.nginx 	192.168.1.13 	    NGINX
* node1.tp4.nfs 	192.168.1.14 	    Serveur NFS